resource "google_dns_managed_zone" "twenty-three-people-zone" {
  project     = module.yaml.variables.project_gcp
  name        = module.yaml.variables.name_dns_zone
  dns_name    = "${module.yaml.variables.name_dns_zone}-${random_id.rnd.hex}.com."
  description = "Example DNS zone"
  labels = {
    foo = "bar"
  }
}

resource "random_id" "rnd" {
  byte_length = 4
}