resource "google_container_cluster" "cluster" {
  project                  = module.yaml.variables.project_gcp
  name                     = module.yaml.variables.name_cluster
  location                 = module.yaml.variables.location_gke
  remove_default_node_pool = true
  initial_node_count       = 1
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  project    = module.yaml.variables.project_gcp
  name       = module.yaml.variables.name_pool_1
  location   = module.yaml.variables.location_gke
  cluster    = google_container_cluster.cluster.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-medium"
    service_account = module.yaml.variables.name_service_account
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}

resource "google_container_node_pool" "secondary_preemptible_nodes" {
  project    = module.yaml.variables.project_gcp
  name       = module.yaml.variables.name_pool_2
  location   = module.yaml.variables.location_gke
  cluster    = google_container_cluster.cluster.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-small"
    service_account = module.yaml.variables.name_service_account
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}