terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.26"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 4.26"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.0"
    }
  }
  required_version = ">= 0.13"
}
