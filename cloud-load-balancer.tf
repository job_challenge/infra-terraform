resource "random_string" "suffix" {
  length  = 8
  upper   = false
  lower   = true
  numeric = true
  special = false
}

locals {
  random_suffix = random_string.suffix.result
  resource_name = "${module.yaml.variables.name_clb}-${local.random_suffix}"
  health_check = {
    type                = "http"
    check_interval_sec  = 1
    healthy_threshold   = 4
    timeout_sec         = 1
    unhealthy_threshold = 5
    response            = ""
    proxy_header        = "NONE"
    port                = 8081
    port_name           = module.yaml.variables.name_port
    request             = ""
    request_path        = "/"
    host                = "1.2.3.4"
    enable_log          = false
  }
}

resource "google_compute_network" "test" {
  project                 = module.yaml.variables.project_gcp
  name                    = local.resource_name
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "test" {
  project       = module.yaml.variables.project_gcp
  name          = local.resource_name
  network       = google_compute_network.test.name
  region        = module.yaml.variables.region_resource
  ip_cidr_range = "10.2.0.0/16"

  secondary_ip_range {
    range_name    = "k8s-pod-range"
    ip_cidr_range = "10.48.0.0/14"
  }
  secondary_ip_range {
    range_name    = "k8s-service-range"
    ip_cidr_range = "10.52.0.0/20"
  }
}

module "test_ilb" {
  source       = "GoogleCloudPlatform/lb-internal/google"
  version      = "~> 5.0"
  project      = module.yaml.variables.project_gcp
  network      = google_compute_network.test.name
  subnetwork   = google_compute_subnetwork.test.name
  region       = module.yaml.variables.region_resource
  name         = local.resource_name
  ports        = ["8080"]
  source_tags  = ["source-tag-foo"]
  target_tags  = ["target-tag-bar"]
  backends     = []
  health_check = local.health_check
}