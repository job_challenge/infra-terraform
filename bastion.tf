module "bastion-host_example_simple_example" {
  source     = "terraform-google-modules/bastion-host/google//examples/simple_example"
  version    = "5.3.0"
  project_id = module.yaml.variables.project_gcp
}