# Terraform_infra_23p

## Prerequisites

Create a service account for deploy:

| Service Account Role |
| --- |
| Kubernetes Engine Cluster Admin |
| Kubernetes Engine admin |
| Kubernetes Engine Developer |

# Service functions

1. Create Bastion 

| Objects |
| --- |
| Service account |
| 1 rule firewall (Allow-bastion-ssh) |
| 1 rule firewall (Allow-ssh-from-api-to-tunel) |
| 1 vpc |
| 1 instance (VM) |
| Bastion instance template |

2. Create zone (DNS) 

3. Ceate Load Balancer

4. Create Cluster

# For deployment GitLab

The first file `request_input.yaml` is composed for one kind of objects. These are:
- *Required Objects* are nodes that must be defined in `request_input.yaml` file.

## Required Objects

```yml
# PROJECT GOOGLE CLOUD PLATFORM
project_gcp: my_project_gcp                # Required
region_resource: my_region                 # Required

# CLOUD LOAD BALANCER - SERVICE
name_clb: my_name_clb                      # Required
name_port: my_name_port                    # Required

# CLOUD DNS
name_dns_zone: my_name_zone                # Required

# SERVICE ACCOUNT
name_service_account: my_service_account   # Required

# GOOGLE KUBERNETES ENGINE
name_cluster: my_name_cluster              # Required
location_gke: my_location                  # Required
name_pool_1: node_1                        # Required
name_pool_2: node_2                        # Required
```

## CI/CD Variables

| Variable | Type |
| --- | --- |
| GCP_KEY | File |
| GCP_PROJECT_DEV | String |
| PROJECT_NAME | String |

# For deployment Local

  1. Instalar [Terraform](http://terraform.io/)

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

```
git clone https://gitlab.com/componentes-de-herramientas/project_23p/terraform_infra_23p.git
```

  2. Add variables in file `request_input.yaml`.
  3. Open terminal.
  4. Run terraform:

  ```bash
    cd terraform_infra_23p
  ```
  ```bash
    terraform init
  ```
  ```bash
    terraform validate
  ```
  ```bash
    terraform plan
  ```
  ```bash
    terraform apply
  ```